# Graphql sample

Run dev:
- In "dev" branch
- Mongodb on 27017
- "pnpm run depend"
- "pnpm run dev"

Run prod:
- In "main" branch
- "pnpm run build"
- "docker-compose up"
