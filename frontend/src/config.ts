export default {
    PUBLIC_URL: String(process.env.NEXT_PUBLIC_GRAPHQL_API_URL),
    URL: String(process.env.GRAPHQL_API_URL || process.env.NEXT_PUBLIC_GRAPHQL_API_URL),
};
