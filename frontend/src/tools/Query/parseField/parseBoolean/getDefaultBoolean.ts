import {
    IBooleanDescription,
    IBooleanResult,
} from './types';

export default function getDefaultBoolean(description: IBooleanDescription): IBooleanResult['value'] {
    return description.default;
}
