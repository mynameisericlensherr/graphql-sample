interface IDescriptionBase {
    type: 'boolean';
}

export interface IDescriptionRequiredNullable extends IDescriptionBase {
    isRequired: true;
    isNullable: true;
    default: boolean | null;
}

export interface IDescriptionRequiredNotNullable extends IDescriptionBase {
    isRequired: true;
    isNullable: false;
    default: boolean;
}

export interface IDescriptionNotRequiredNullable extends IDescriptionBase {
    isRequired: false;
    isNullable: true;
    default: boolean | undefined | null;
}

export interface IDescriptionNotRequiredNotNullable extends IDescriptionBase {
    isRequired: false;
    isNullable: false;
    default: boolean | undefined;
}

export type IBooleanDescription =
    IDescriptionRequiredNullable |
    IDescriptionRequiredNotNullable |
    IDescriptionNotRequiredNullable |
    IDescriptionNotRequiredNotNullable;

export interface IResultRequiredNullable {
    value: boolean | null;
}

export interface IResultRequiredNotNullable {
    value: boolean;
}

export interface IResultNotRequiredNullable {
    value: boolean | undefined | null;
}

export interface IResultNotRequiredNotNullable {
    value: boolean | undefined;
}

export type IBooleanResult =
    IResultRequiredNullable |
    IResultRequiredNotNullable |
    IResultNotRequiredNullable |
    IResultNotRequiredNotNullable;
