import {
    IBooleanDescription,
    IBooleanResult,
} from './parseBoolean/types';
import {
    IDateDescription,
    IDateResult,
} from './parseDate/types';
import {
    INumberDescription,
    INumberResult,
} from './parseNumber/types';
import {
    IStringDescription,
    IStringResult,
} from './parseString/types';

export type IFieldType =
    IBooleanDescription['type'] |
    INumberDescription['type'] |
    IStringDescription['type'] |
    IDateDescription['type'];

export type IFieldDescription<IType extends IFieldType> =
    IType extends 'boolean' ? IBooleanDescription :
        IType extends 'number' ? INumberDescription :
            IType extends 'string' ? IStringDescription :
                IType extends 'date' ? IDateDescription :
                    never;

export type IFieldDescriptionByValue<IValue> =
    IValue extends boolean ? IBooleanDescription :
        IValue extends number ? INumberDescription :
            IValue extends string ? IStringDescription :
                IValue extends Date ? IDateDescription :
                    never;

export type IFieldResult<IType extends IFieldType> =
    IType extends 'boolean' ? IBooleanResult :
        IType extends 'number' ? INumberResult :
            IType extends 'string' ? IStringResult :
                IType extends 'date' ? IDateResult :
                    never;

export type IFieldResultValue<IType extends IFieldType> =
    IFieldResult<IType>['value'];
