interface IDescriptionBase {
    type: 'number';

    enum?: number[];
    isFloat?: boolean;
}

export interface IDescriptionRequiredNullable extends IDescriptionBase {
    isRequired: true;
    isNullable: true;
    default: number | null;
}

export interface IDescriptionRequiredNotNullable extends IDescriptionBase {
    isRequired: true;
    isNullable: false;
    default: number;
}

export interface IDescriptionNotRequiredNullable extends IDescriptionBase {
    isRequired: false;
    isNullable: true;
    default: number | undefined | null;
}

export interface IDescriptionNotRequiredNotNullable extends IDescriptionBase {
    isRequired: false;
    isNullable: false;
    default: number | undefined;
}

export type INumberDescription =
    IDescriptionRequiredNullable |
    IDescriptionRequiredNotNullable |
    IDescriptionNotRequiredNullable |
    IDescriptionNotRequiredNotNullable;

export interface IResultRequiredNullable {
    value: number | null;
}

export interface IResultRequiredNotNullable {
    value: number;
}

export interface IResultNotRequiredNullable {
    value: number | undefined | null;
}

export interface IResultNotRequiredNotNullable {
    value: number | undefined;
}

export type INumberResult =
    IResultRequiredNullable |
    IResultRequiredNotNullable |
    IResultNotRequiredNullable |
    IResultNotRequiredNotNullable;
