import {
    INumberDescription,
    INumberResult,
} from './types';

export default function getDefaultNumber(description: INumberDescription): INumberResult['value'] {
    return description.default;
}
