import {
    IStringDescription,
    IStringResult,
} from './types';

export default function getDefaultString(description: IStringDescription): IStringResult['value'] {
    return description.default;
}
