interface IDescriptionBase {
    type: 'string';

    enum?: string[];
}

export interface IDescriptionRequiredNullable extends IDescriptionBase {
    isRequired: true;
    isNullable: true;
    default: string | null;
}

export interface IDescriptionRequiredNotNullable extends IDescriptionBase {
    isRequired: true;
    isNullable: false;
    default: string;
}

export interface IDescriptionNotRequiredNullable extends IDescriptionBase {
    isRequired: false;
    isNullable: true;
    default: string | undefined | null;
}

export interface IDescriptionNotRequiredNotNullable extends IDescriptionBase {
    isRequired: false;
    isNullable: false;
    default: string | undefined;
}

export type IStringDescription =
    IDescriptionRequiredNullable |
    IDescriptionRequiredNotNullable |
    IDescriptionNotRequiredNullable |
    IDescriptionNotRequiredNotNullable;

export interface IResultRequiredNullable {
    value: string | null;
}

export interface IResultRequiredNotNullable {
    value: string;
}

export interface IResultNotRequiredNullable {
    value: string | undefined | null;
}

export interface IResultNotRequiredNotNullable {
    value: string | undefined;
}

export type IStringResult =
    IResultRequiredNullable |
    IResultRequiredNotNullable |
    IResultNotRequiredNullable |
    IResultNotRequiredNotNullable;
