interface IDescriptionBase {
    type: 'date';
}

export interface IDescriptionRequiredNullable extends IDescriptionBase {
    isRequired: true;
    isNullable: true;
    default(): Date | null;
}

export interface IDescriptionRequiredNotNullable extends IDescriptionBase {
    isRequired: true;
    isNullable: false;
    default(): Date;
}

export interface IDescriptionNotRequiredNullable extends IDescriptionBase {
    isRequired: false;
    isNullable: true;
    default(): Date | undefined | null;
}

export interface IDescriptionNotRequiredNotNullable extends IDescriptionBase {
    isRequired: false;
    isNullable: false;
    default(): Date | undefined;
}

export type IDateDescription =
    IDescriptionRequiredNullable |
    IDescriptionRequiredNotNullable |
    IDescriptionNotRequiredNullable |
    IDescriptionNotRequiredNotNullable;

export interface IResultRequiredNullable {
    value: Date | null;
}

export interface IResultRequiredNotNullable {
    value: Date;
}

export interface IResultNotRequiredNullable {
    value: Date | undefined | null;
}

export interface IResultNotRequiredNotNullable {
    value: Date | undefined;
}

export type IDateResult =
    IResultRequiredNullable |
    IResultRequiredNotNullable |
    IResultNotRequiredNullable |
    IResultNotRequiredNotNullable;
