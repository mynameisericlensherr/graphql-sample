import {
    IDateDescription,
    IDateResult,
} from './types';

export default function getDefaultDate(description: IDateDescription): IDateResult['value'] {
    return description.default();
}
