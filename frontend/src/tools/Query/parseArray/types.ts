import {
    IFieldDescription,
    IFieldDescriptionByValue,
    IFieldResult,
    IFieldType,
} from '../parseField/types';

interface IDescriptionBase<IType extends IFieldType> {
    type: 'array';
    default: IFieldResult<IType>['value'][];
    items: IFieldDescription<IType>;
}

export interface IDescriptionRequiredNullable<IType extends IFieldType> extends IDescriptionBase<IType> {
    isRequired: true;
    isNullable: true;
}

export interface IDescriptionRequiredNotNullable<IType extends IFieldType> extends IDescriptionBase<IType> {
    isRequired: true;
    isNullable: false;
}

export interface IDescriptionNotRequiredNullable<IType extends IFieldType> extends IDescriptionBase<IType> {
    isRequired: false;
    isNullable: true;
}

export interface IDescriptionNotRequiredNotNullable<IType extends IFieldType> extends IDescriptionBase<IType> {
    isRequired: false;
    isNullable: false;
}

export type IArrayDescription<IType extends IFieldType> =
    IDescriptionRequiredNullable<IType> |
    IDescriptionRequiredNotNullable<IType> |
    IDescriptionNotRequiredNullable<IType> |
    IDescriptionNotRequiredNotNullable<IType>;

export type IArrayDescriptionByValue<IValue> = IFieldDescriptionByValue<IValue>[];

export interface IResultRequiredNullable<IType extends IFieldType> {
    value: IFieldResult<IType>['value'][] | null;
}

export interface IResultRequiredNotNullable<IType extends IFieldType> {
    value: IFieldResult<IType>['value'][];
}

export interface IResultNotRequiredNullable<IType extends IFieldType> {
    value: IFieldResult<IType>['value'][] | undefined | null;
}

export interface IResultNotRequiredNotNullable<IType extends IFieldType> {
    value: IFieldResult<IType>['value'][] | undefined;
}

export type IArrayResult<IType extends IFieldType> =
    IResultRequiredNullable<IType> |
    IResultRequiredNotNullable<IType> |
    IResultNotRequiredNullable<IType> |
    IResultNotRequiredNotNullable<IType>;
