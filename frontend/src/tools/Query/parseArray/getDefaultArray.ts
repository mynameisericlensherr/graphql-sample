import {
    IArrayDescription,
    IArrayResult,
} from './types';
import {
    IFieldType,
} from '../parseField/types';

export default function getDefaultArray<IType extends IFieldType>(description: IArrayDescription<IType>): IArrayResult<IType>['value'] {
    return description.default;
}
