export function isNeedToRedirectOnRemove(skip: number, usersTotal: number) {
    if (!skip) {
        return false;
    }

    return skip === usersTotal;
}
