export interface IUser {
    id: string;

    name: string;
    photo: string;
    email: string;

    photoUrl: string;
}
