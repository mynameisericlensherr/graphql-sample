import {
    IWithError,
} from 'instances/types';

import {
    IUser,
} from '.';

export interface IGet extends IWithError {
    users?: IUser[];
    total?: number;
}

export interface IGetOne extends IWithError {
    user?: IUser;
}

export interface IPost extends IWithError {
    user?: IUser;
}

export interface IPatch extends IWithError {
    user?: IUser;
}

export interface IRemove extends IWithError {

}
