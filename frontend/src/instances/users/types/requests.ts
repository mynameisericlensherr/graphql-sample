export interface IGetQuery {
    skip: number;
    limit: number;
}

export interface IGetOnePath {
    id: string;
}

export interface IPostBody {
    name: string;
    photo: string;
    email: string;
}

export interface IPatchBody {
    id: string;

    name: string;
    photo: string;
    email: string;
}

export interface IRemovePath {
    id: string;
}
