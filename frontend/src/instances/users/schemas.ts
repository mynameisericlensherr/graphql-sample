import {
    gql,
} from '@apollo/client';

export const GET = gql`
    query Users($skip: Int!, $limit: Int!) {
        users(skip: $skip, limit: $limit) {
            users {
                id
                name
                photo
                email

                photoUrl
            }
            total
            error
        }
    }
`;

export const GET_ONE = gql`
    query User($id: ID!) {
        user(id: $id) {
            user {
                id
                name
                photo
                email

                photoUrl
            }
            error
        }
    }
`;

export const POST = gql`
    mutation AddUser($name: String!, $photo: ID!, $email: String!) {
        addUser(name: $name, photo: $photo, email: $email) {
            user {
                id
                name
                photo
                email
            }
            error
        }
    }
`;

export const PATCH = gql`
    mutation UpdateUser($id: ID!, $name: String!, $photo: ID!, $email: String!) {
        updateUser(id: $id, name: $name, photo: $photo, email: $email) {
            user {
                id
                name
                photo
                email
            }
            error
        }
    }
`;

export const REMOVE = gql`
    mutation RemoveUser($id: ID!) {
        removeUser(id: $id) {
            error
        }
    }
`;
