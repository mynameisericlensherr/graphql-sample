import {
    IUser,
} from './types';
import {
    IGetQuery,
    IRemovePath,
} from './types/requests';
import {
    IGet, IRemove,
} from './types/responses';

import {
    useRouter,
} from 'next/router';
import {
    useEffect,
    useState,
} from 'react';

import Query from 'tools/Query';

import * as fetches from './fetches';
import {
    isNeedToRedirectOnRemove,
} from './functions';

export function useUsers(initData: IGet) {
    const [users, setUsers] = useState<IUser[] | null>(initData.users || null);
    const [usersTotal, setUsersTotal] = useState<number | null>(initData.total || null);

    const router = useRouter();

    const removeUser = async (removePath: IRemovePath, getQuery: IGetQuery): Promise<IRemove> => {
        const removeRes = await fetches.remove(removePath);

        if (removeRes.error) {
            return removeRes;
        }

        const getRes = await fetches.get({
            ...getQuery,
            skip: getQuery.skip + getQuery.limit - 1,
            limit: 1,
        });

        if (!getRes.users || getRes.total === undefined) {
            return removeRes;
        }
        if (!users) {
            return removeRes;
        }
        if (isNeedToRedirectOnRemove(getQuery.skip, getRes.total)) {
            await router.push(Query.stringify({
                skip: getQuery.skip - getQuery.limit,
                limit: getQuery.limit,
            }));

            return removeRes;
        }

        setUsers([...users.filter((user) => user.id !== removePath.id), ...getRes.users]);
        setUsersTotal((usersTotal || 1) - 1);

        return removeRes;
    };

    useEffect(() => {
        setUsers(initData.users || null);
        setUsersTotal(initData.total || null);
    }, [initData]);

    return {
        users,
        usersTotal,
        removeUser,
    };
}
