import {
    IGetOnePath,
    IGetQuery,
    IPatchBody,
    IPostBody,
    IRemovePath,
} from './types/requests';
import {
    IGet,
    IGetOne,
    IPatch,
    IPost,
    IRemove,
} from './types/responses';

import {
    HttpLink,
} from '@apollo/client';

import config from '../../config';
import graphql from '../../graphql';

import {
    INSTANCE_PATH,
} from './constants';
import {
    GET,
    GET_ONE,
    PATCH,
    POST,
    REMOVE,
} from './schemas';

export async function get(query: IGetQuery): Promise<IGet> {
    const link = new HttpLink({
        uri: `${config.URL}${INSTANCE_PATH}`,
    });

    graphql.setLink(link);

    const {
        data,
    } = await graphql.query({
        query: GET,
        variables: query,
    });

    if (data.users.error) {
        console.log(data.users.error);
    }

    return data.users;
}

export async function getOne(path: IGetOnePath): Promise<IGetOne> {
    const link = new HttpLink({
        uri: `${config.URL}${INSTANCE_PATH}`,
    });

    graphql.setLink(link);

    const {
        data,
    } = await graphql.query({
        query: GET_ONE,
        variables: path,
    });

    if (data.user.error) {
        console.log(data.user.error);
    }

    return data.user;
}

export async function post(body: IPostBody): Promise<IPost> {
    const link = new HttpLink({
        uri: `${config.PUBLIC_URL}${INSTANCE_PATH}`,
    });

    graphql.setLink(link);

    const {
        data,
    } = await graphql.mutate({
        mutation: POST,
        variables: body,
    });

    if (data.addUser.error) {
        console.log(data.addUser.error);
    }

    return data.addUser;
}

export async function patch(body: IPatchBody): Promise<IPatch> {
    const link = new HttpLink({
        uri: `${config.PUBLIC_URL}${INSTANCE_PATH}`,
    });

    graphql.setLink(link);

    const {
        data,
    } = await graphql.mutate({
        mutation: PATCH,
        variables: body,
    });

    if (data.updateUser.error) {
        console.log(data.updateUser.error);
    }

    return data.updateUser;
}

export async function remove(path: IRemovePath): Promise<IRemove> {
    const link = new HttpLink({
        uri: `${config.PUBLIC_URL}${INSTANCE_PATH}`,
    });

    graphql.setLink(link);

    const {
        data,
    } = await graphql.mutate({
        mutation: REMOVE,
        variables: path,
    });

    if (data.removeUser.error) {
        console.log(data.removeUser.error);
    }

    return data.removeUser;
}
