import {
    IWithError,
} from 'instances/types';

import {
    IUserPhoto,
} from '.';

export interface IPost extends IWithError {
    userPhoto?: IUserPhoto;
}
