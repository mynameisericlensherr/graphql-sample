import {
    IUserPhoto,
} from './types';
import {
    IPostBody,
} from './types/request';
import {
    IPost,
} from './types/responses';

import {
    useState,
} from 'react';

import * as fetches from './fetches';

export function useUserPhotos(defaultUserPhoto: IUserPhoto | null) {
    const [userPhoto, setUserPhoto] = useState<IUserPhoto | null>(defaultUserPhoto);

    const postUserPhoto = async (body: IPostBody): Promise<IPost> => {
        const res = await fetches.post(body);

        if (!res.userPhoto || res.error) {
            console.log(res.error);

            return res;
        }

        setUserPhoto(res.userPhoto);

        return res;
    };

    return {
        userPhoto,
        postUserPhoto,
    };
}
