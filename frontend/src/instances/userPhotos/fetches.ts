import {
    IPostBody,
} from './types/request';
import {
    IPost,
} from './types/responses';

import config from 'config';

import Fetch from 'tools/Fetch';

import {
    INSTANCE_PATH,
} from './constants';

export async function post(body: IPostBody): Promise<IPost> {
    const formData = new FormData();

    formData.set('photo', body.file);

    return new Fetch({
        url: `${config.PUBLIC_URL}${INSTANCE_PATH}`,
        method: 'POST',
        credentials: 'omit',
        formData,
    })
        .on([200], (body) => {
            return body;
        })
        .on([400, 500], (body) => {
            return body;
        })
        .exec();
}
