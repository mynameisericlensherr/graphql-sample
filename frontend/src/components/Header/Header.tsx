import {
    Button,
    Layout,
    Typography,
} from 'antd';
import Link from 'next/link';
import React, {
    FC,
} from 'react';

import styles from './Header.module.scss';

const Header: FC = () => {
    return (
        <Layout.Header className={styles.header}>
            <Typography.Title
                className={styles.title}
                level={2}
            >
                Amazing header title
            </Typography.Title>
            <Link href={'/'}>
                <Button
                    type={'default'}
                    size={'large'}
                >
                    View users
                </Button>
            </Link>
        </Layout.Header>
    );
};

export default Header;
