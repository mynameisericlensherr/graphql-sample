import {
    Typography,
} from 'antd';
import React, {
    FC,
} from 'react';

import styles from './NotFound.module.scss';

const NotFound: FC = () => {
    return (
        <div className={styles.notFound}>
            <Typography.Title level={2}>
                Not found
            </Typography.Title>
        </div>
    );
};

export default NotFound;
