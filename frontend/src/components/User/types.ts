import {
    IUser,
} from 'instances/users/types';

export interface IProps {
    isNew: boolean;
    user: IUser | null;
}
