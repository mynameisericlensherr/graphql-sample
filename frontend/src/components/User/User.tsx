import {
    IProps,
} from './types';

import {
    Button,
    Card,
} from 'antd';
import {
    useRouter,
} from 'next/router';
import React, {
    FC,
    useState,
} from 'react';

import * as fetches from 'instances/users/fetches';

import NotFound from 'components/NotFound';

import UserForm, {
    useUserFormRef,
} from './UserForm';

import styles from './User.module.scss';

const User: FC<IProps> = (props) => {
    const [isPending, setIsPending] = useState(false);

    const router = useRouter();

    const userFormRef = useUserFormRef();

    const onSave = async () => {
        if (isPending) {
            return;
        }
        if (!userFormRef.current.validate()) {
            return;
        }
        if (props.user) {
            const body = userFormRef.current.getPatchBody();

            if (!body) {
                return;
            }

            setIsPending(true);

            const res = await fetches.patch(body);

            setIsPending(false);

            if (!res.user || res.error) {
                return;
            }
        } else {
            const body = userFormRef.current.getPostBody();

            if (!body) {
                return;
            }

            setIsPending(true);

            const res = await fetches.post(body);

            setIsPending(false);

            if (!res.user || res.error) {
                return;
            }

            await router.push(`/users/${res.user.id}`);
        }

        alert('Succeed!');
    };

    if (!props.user && !props.isNew) {
        return (
            <NotFound/>
        );
    }

    return (
        <div className={styles.user}>
            <div className={styles.cardContainer}>
                <Card
                    className={styles.card}
                    bordered={true}
                    onKeyDown={(e) => e.key === 'Enter' && onSave()}
                >
                    <UserForm
                        ref={userFormRef}
                        user={props.user}
                    />
                    <div className={styles.buttonContainer}>
                        <Button
                            type={'primary'}
                            size={'large'}
                            disabled={isPending}
                            onClick={onSave}
                        >
                            Save
                        </Button>
                    </div>
                </Card>
            </div>
        </div>
    );
};

export default User;
