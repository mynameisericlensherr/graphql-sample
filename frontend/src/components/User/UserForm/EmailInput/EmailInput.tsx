import {
    IProps,
    IRef,
} from './types';

import {
    Form,
    Input,
    Typography,
} from 'antd';
import React, {
    forwardRef,
    useImperativeHandle,
    useState,
} from 'react';

import {
    checkEmail,
} from 'helpers/functions';

import {
    ERROR,
} from './constants';
import {
    formatError,
} from './functions';

const EmailInput = forwardRef<IRef, IProps>((props, ref) => {
    const [value, setValue] = useState(props.value);
    const [error, setError] = useState<ERROR | null>(null);

    const onChange = (value: string) => {
        setValue(value);
        setError(null);
    };

    useImperativeHandle(ref, () => {
        return {
            getValue(): string {
                return value;
            },
            validate(): boolean {
                if (!value) {
                    setError(ERROR.IS_REQUIRED);

                    return false;
                }
                if (!checkEmail(value)) {
                    setError(ERROR.INVALID_FORMAT);

                    return false;
                }

                setError(null);

                return true;
            },
        };
    });

    return (
        <Form.Item label={'Email'}>
            <Input
                value={value}
                status={error ? 'error' : undefined}
                onChange={(e) => onChange(e.target.value)}
            />
            {
                error &&
                <Typography.Text type={'danger'}>
                    {formatError(error)}
                </Typography.Text>
            }
        </Form.Item>
    );
});

export default EmailInput;
