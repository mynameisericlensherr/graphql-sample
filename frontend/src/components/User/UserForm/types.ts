import {
    IUser,
} from 'instances/users/types';
import {
    IPatchBody,
    IPostBody,
} from 'instances/users/types/requests';

export interface IProps {
    user: IUser | null;
}

export interface IRef {
    getPostBody(): IPostBody | null;
    getPatchBody(): IPatchBody | null;
    validate(): boolean;
}
