export interface IProps {
    value: string;
}

export interface IRef {
    getValue(): string;
    validate(): boolean;
}
