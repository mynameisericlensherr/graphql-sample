import {
    IRef,
} from './types';

import {
    useDefaultedRef,
} from 'tools/hooks';

import {
    ERROR,
} from './constants';

export function useNameInputRef() {
    return useDefaultedRef<IRef>({
        getValue(): string {
            return '';
        },
        validate(): boolean {
            return true;
        },
    });
}

export function formatError(error: ERROR | null): string {
    switch (error) {
        case ERROR.IS_REQUIRED: {
            return 'Required';
        }
        case null: {
            return '';
        }
    }
}
