import {
    IRef,
} from './types';
import {
    IPatchBody,
    IPostBody,
} from 'instances/users/types/requests';

import {
    useDefaultedRef,
} from 'tools/hooks';

export function useUserFormRef() {
    return useDefaultedRef<IRef>({
        getPostBody(): IPostBody | null {
            return null;
        },
        getPatchBody(): IPatchBody | null {
            return null;
        },
        validate(): boolean {
            return true;
        },
    });
}
