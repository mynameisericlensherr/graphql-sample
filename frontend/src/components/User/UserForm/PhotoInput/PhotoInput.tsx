import {
    IProps,
    IRef,
} from './types';

import {
    Button,
    Image, Typography,
} from 'antd';
import classNames from 'classnames';
import React, {
    forwardRef,
    useImperativeHandle,
    useState,
} from 'react';

import {
    chooseFile,
} from 'helpers/functions';

import {
    useUserPhotos,
} from 'instances/userPhotos/hooks';

import {
    ERROR,
} from './constants';
import {
    formatError,
} from './functions';

import addImage from './media/add.png';

import styles from './PhotoInput.module.scss';

const PhotoInput = forwardRef<IRef, IProps>((props, ref) => {
    const [error, setError] = useState<ERROR | null>(null);
    const [isPending, setIsPending] = useState(false);

    const {
        userPhoto,
        postUserPhoto,
    } = useUserPhotos(props.value);

    const onChoose = async () => {
        const file = await chooseFile('images');

        if (!file) {
            return;
        }

        setIsPending(true);
        await postUserPhoto({
            file,
        });
        setIsPending(false);
        setError(null);
    };

    useImperativeHandle(ref, () => {
        return {
            getValue(): string {
                return userPhoto?.id || '';
            },
            validate(): boolean {
                if (!userPhoto) {
                    setError(ERROR.IS_REQUIRED);

                    return false;
                }

                setError(null);

                return true;
            },
        };
    });

    return (
        <div className={classNames(props.className, styles.photoInput)}>
            {
                userPhoto ?
                    <Image
                        src={userPhoto.url}
                        width={150}
                    /> :
                    <img
                        className={styles.uploadImage}
                        src={addImage.src}
                        alt={''}
                        width={150}
                        onClick={onChoose}
                    />
            }
            <Button
                type={'default'}
                size={'small'}
                disabled={isPending}
                onClick={onChoose}
            >
                Choose
            </Button>
            {
                error &&
                <Typography.Text type={'danger'}>
                    {formatError(error)}
                </Typography.Text>
            }
        </div>
    );
});

export default PhotoInput;
