import {
    IUserPhoto,
} from 'instances/userPhotos/types';

export interface IProps {
    value: IUserPhoto | null;

    className?: string;
}

export interface IRef {
    getValue(): string;
    validate(): boolean;
}
