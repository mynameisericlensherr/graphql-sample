import {
    IProps,
    IRef,
} from './types';
import {
    IPatchBody,
    IPostBody,
} from 'instances/users/types/requests';

import React, {
    forwardRef,
    useImperativeHandle,
} from 'react';

import EmailInput, {
    useEmailInputRef,
} from './EmailInput';
import NameInput, {
    useNameInputRef,
} from './NameInput';
import PhotoInput, {
    usePhotoInputRef,
} from './PhotoInput';

import styles from './UserForm.module.scss';

const UserForm = forwardRef<IRef, IProps>((props, ref) => {
    const nameInputRef = useNameInputRef();
    const photoInputRef = usePhotoInputRef();
    const emailInputRef = useEmailInputRef();

    useImperativeHandle(ref, () => {
        return {
            getPostBody(): IPostBody | null {
                if (props.user) {
                    return null;
                }

                return {
                    name: nameInputRef.current.getValue(),
                    photo: photoInputRef.current.getValue(),
                    email: emailInputRef.current.getValue(),
                };
            },
            getPatchBody(): IPatchBody | null {
                if (!props.user) {
                    return null;
                }

                return {
                    id: props.user.id,

                    name: nameInputRef.current.getValue(),
                    photo: photoInputRef.current.getValue(),
                    email: emailInputRef.current.getValue(),
                };
            },
            validate(): boolean {
                return ![
                    nameInputRef.current.validate(),
                    photoInputRef.current.validate(),
                    emailInputRef.current.validate(),
                ].includes(false);
            },
        };
    });

    return (
        <div className={styles.userForm}>
            <PhotoInput
                ref={photoInputRef}
                className={styles.photoInput}
                value={
                    props.user ?
                        {
                            id: props.user.photo,
                            url: props.user.photoUrl,
                        } :
                        null
                }
            />
            <div>
                <div>
                    <NameInput
                        ref={nameInputRef}
                        value={props.user?.name || ''}
                    />
                </div>
                <div>
                    <EmailInput
                        ref={emailInputRef}
                        value={props.user?.email || ''}
                    />
                </div>
            </div>
        </div>
    );
});

export default UserForm;
