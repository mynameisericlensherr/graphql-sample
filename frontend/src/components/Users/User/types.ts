import {
    IUser,
} from 'instances/users/types';
import {
    IRemovePath,
} from 'instances/users/types/requests';
import {
    IRemove,
} from 'instances/users/types/responses';

export interface IProps {
    user: IUser;

    removeUser(path: IRemovePath): Promise<IRemove>;
}
