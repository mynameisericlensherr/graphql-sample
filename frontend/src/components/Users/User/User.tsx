import {
    IProps,
} from './types';

import {
    Avatar,
    Button,
    Col,
    Row,
    Typography,
} from 'antd';
import Link from 'next/link';
import React, {
    FC, useState,
} from 'react';

import styles from './User.module.scss';

const User: FC<IProps> = (props) => {
    const [isPending, setIsPending] = useState(false);

    const onRemove = async () => {
        if (isPending) {
            return;
        }
        if (!confirm('Sure?')) {
            return;
        }

        setIsPending(true);

        const res = await props.removeUser({
            id: props.user.id,
        });

        if (res.error) {
            setIsPending(false);

            return;
        }

        setIsPending(false);
    };

    return (
        <Row className={styles.user}>
            <Col span={2}>
                <Avatar
                    src={props.user.photoUrl}
                    size={'default'}
                />
            </Col>
            <Col
                className={styles.col}
                span={9}
            >
                <Typography.Title
                    className={styles.colText}
                    level={5}
                >
                    {props.user.name}
                </Typography.Title>
            </Col>
            <Col
                className={styles.col}
                span={9}
            >
                <Typography.Title
                    className={styles.colText}
                    level={5}
                >
                    {props.user.email}
                </Typography.Title>
            </Col>
            <Col
                className={styles.actions}
                span={4}
            >
                <Button type={'primary'}>
                    <Link href={`/users/${props.user.id}`}>
                        Edit
                    </Link>
                </Button>
                <Button
                    type={'primary'}
                    danger={true}
                    disabled={isPending}
                    onClick={onRemove}
                >
                    Remove
                </Button>
            </Col>
        </Row>
    );
};

export default User;
