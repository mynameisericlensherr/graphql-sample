import {
    IProps,
} from './types';

import {
    Button,
    List,
    Typography,
} from 'antd';
import Link from 'next/link';
import React, {
    FC,
} from 'react';

import {
    NEW_PATH_ID,
} from 'instances/users/constants';
import {
    useUsers,
} from 'instances/users/hooks';

import Pagination from './Pagination';
import User from './User';

import styles from './Users.module.scss';

const Users: FC<IProps> = (props) => {
    const {
        users,
        usersTotal,
        removeUser,
    } = useUsers(props.res);

    return (
        <div className={styles.users}>
            <List
                bordered={true}
                dataSource={[{}]}
                renderItem={
                    () =>
                        <List.Item
                            key={0}
                            className={styles.create}
                        >
                            <Button type={'primary'}>
                                <Link href={`/users/${NEW_PATH_ID}`}>
                                    Create
                                </Link>
                            </Button>
                        </List.Item>
                }
            />
            {
                users &&
                usersTotal ?
                    <List
                        bordered={true}
                        dataSource={users}
                        renderItem={
                            (user) =>
                                <List.Item key={user.id}>
                                    <User
                                        key={user.id}
                                        user={user}
                                        removeUser={(path) => removeUser(path, props.query)}
                                    />
                                </List.Item>
                        }
                    /> :
                    <div className={styles.nobody}>
                        <Typography.Title level={3}>
                            Nobody to show :С
                        </Typography.Title>
                    </div>
            }
            {
                !!usersTotal &&
                <Pagination
                    className={styles.pagination}
                    query={props.query}
                    usersTotal={usersTotal}
                />
            }
        </div>
    );
};

export default Users;
