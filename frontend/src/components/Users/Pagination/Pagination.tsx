import {
    IProps,
} from './types';

import {
    Button,
} from 'antd';
import classNames from 'classnames';
import Link from 'next/link';
import React, {
    FC,
} from 'react';

import Query from 'tools/Query';

import styles from './Pagination.module.scss';

const Pagination: FC<IProps> = (props) => {
    const currentPage = Math.floor(props.query.skip / props.query.limit) + 1;
    const pagesCount = Math.ceil(props.usersTotal / props.query.limit);
    const isNoUsers = props.query.skip > props.usersTotal;

    return (
        <div className={classNames(styles.pagination, props.className)}>
            <Link
                href={Query.stringify({
                    ...props.query,
                    skip: props.query.skip - props.query.limit,
                })}
            >
                <Button
                    className={styles.button}
                    size={'large'}
                    disabled={currentPage === 1 || isNoUsers}
                >
                    Back
                </Button>
            </Link>
            <Link
                href={Query.stringify({
                    ...props.query,
                    skip: props.query.skip + props.query.limit,
                })}
            >
                <Button
                    className={styles.button}
                    size={'large'}
                    disabled={currentPage === pagesCount || isNoUsers}
                >
                    Forward
                </Button>
            </Link>
        </div>
    );
};

export default Pagination;
