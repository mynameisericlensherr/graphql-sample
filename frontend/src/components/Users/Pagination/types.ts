import {
    IGetQuery,
} from 'instances/users/types/requests';

export interface IProps {
    query: IGetQuery;
    usersTotal: number;

    className?: string;
}
