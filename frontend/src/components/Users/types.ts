import {
    IGetQuery,
} from 'instances/users/types/requests';
import {
    IGet,
} from 'instances/users/types/responses';

export interface IProps {
    query: IGetQuery;
    res: IGet;
}
