export default function(acceptTypes: 'images' | 'any'): Promise<File | null> {
    return new Promise<File | null>((resolve) => {
        const input = document.createElement('input');
        let accept = '';

        switch (acceptTypes) {
            case 'images': {
                accept = '.jpg,.jpeg,.png';
                break;
            }
            case 'any': {
                break;
            }
        }

        input.type = 'file';
        input.accept = accept;
        input.onchange = (e) => {
            // @ts-ignore
            resolve(e.target?.files?.[0] || null);
        };

        input.click();
    });
}
