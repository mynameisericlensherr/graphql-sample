import {
    ApolloProvider,
} from '@apollo/client';
import {
    Layout,
} from 'antd';
import React from 'react';

import Header from 'components/Header';

import 'antd/dist/antd.css';
import graphql from '../graphql';

import 'index.scss';

export default function app(props) {
    return (
        <ApolloProvider client={graphql}>
            <Layout>
                <Layout.Content className={'content'}>
                    <Header/>
                    <props.Component {...props.pageProps}/>
                </Layout.Content>
            </Layout>
        </ApolloProvider>
    );
}
