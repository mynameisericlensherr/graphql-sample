import React from 'react';

import {
    NEW_PATH_ID,
} from 'instances/users/constants';
import * as fetches from 'instances/users/fetches';

import User from 'components/User';

export default function user(props) {
    return (
        <User {...props}/>
    );
}

export async function getServerSideProps(props) {
    const {
        id,
    } = props.params;

    if (id === NEW_PATH_ID) {
        return {
            props: {
                isNew: true,
                user: null,
            },
        };
    }

    const res = await fetches.getOne({
        id,
    });

    return {
        props: {
            isNew: false,
            user: res.user || null,
        },
    };
}
