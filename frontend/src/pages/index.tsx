import {
    IGetQuery,
} from 'instances/users/types/requests';

import React from 'react';

import Query from 'tools/Query';

import {
    PAGE_SIZE,
} from 'instances/users/constants';
import * as fetches from 'instances/users/fetches';

import Users from 'components/Users';

export default function index(props) {
    return (
        <Users {...props}/>
    );
}

export async function getServerSideProps(context) {
    // Does not context provide pure query? Lol.
    const query = Query.parse<IGetQuery>(Query.stringify(context.query), {
        skip: {
            type: 'number',
            isRequired: true,
            isNullable: false,
            default: 0,
        },
        limit: {
            type: 'number',
            isRequired: true,
            isNullable: false,
            default: PAGE_SIZE,
        },
    });
    const res = await fetches.get(query);

    return {
        props: {
            query,
            res,
        },
    };
}
