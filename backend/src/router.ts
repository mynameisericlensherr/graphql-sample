import {
    Router,
} from 'express';

import {
    USER_PHOTOS_PATH,
} from './instances/userPhotos/constants';
import userPhotosRouter from './instances/userPhotos/router';
import {
    USERS_PATH,
} from './instances/users/constants';
import usersRouter from './instances/users/router';

const router = Router();

router.use(USER_PHOTOS_PATH, userPhotosRouter);
router.use(USERS_PATH, usersRouter);

export default router;
