import {
    Router,
} from 'express';
import {
    graphqlHTTP,
} from 'express-graphql';

import rootValue from './rootValue';
import schema from './schema';

const usersRouter = Router();

usersRouter.use(
    '/',
    graphqlHTTP((req, res) => {
        return {
            schema,
            rootValue,
            graphiql: true,
            context: {
                req,
                res,
            },
        };
    })
);

export default usersRouter;
