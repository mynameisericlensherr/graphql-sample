import {
    IPatchBody,
    IPostBody,
} from './types/requests';

import {
    checkEmail,
} from 'helpers/functions';

import {
    UserPhotos,
} from 'instances/userPhotos/model';

import {
    RESPONSE_ERROR,
} from './constants';

export async function validatePostBody(body: IPostBody): Promise<RESPONSE_ERROR | null> {
    if (!checkEmail(body.email)) {
        return RESPONSE_ERROR.INVALID_EMAIL;
    }
    if (!await UserPhotos.existsById(body.photo)) {
        return RESPONSE_ERROR.USER_PHOTO_NOT_FOUND;
    }

    return null;
}

export async function validatePatchBody(body: IPatchBody): Promise<RESPONSE_ERROR | null> {
    if (!checkEmail(body.email)) {
        return RESPONSE_ERROR.INVALID_EMAIL;
    }
    if (!await UserPhotos.existsById(body.photo)) {
        return RESPONSE_ERROR.USER_PHOTO_NOT_FOUND;
    }

    return null;
}
