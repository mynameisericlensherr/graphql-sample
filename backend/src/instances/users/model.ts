import {
    IDocument,
    IFindByQueryResult,
    IModel, IObject,
} from './types';
import {
    IGetQuery,
} from './types/requests';

import {
    model,
    Schema,
} from 'mongoose';

import Mongo from 'tools/Mongo';

import {
    USER_PHOTOS_COLLECTION_NAME,
} from 'instances/userPhotos/constants';

import {
    getUserPhotoUrl,
} from '../userPhotos/functions';

import {
    USERS_COLLECTION_NAME,
} from './constants';

const {
    Types,
} = Schema;

const UsersSchema = new Schema<IDocument, IModel>({
    name: {
        type: Types.String,
        required: true,
    },
    photo: {
        type: Types.ObjectId,
        ref: USER_PHOTOS_COLLECTION_NAME,
        required: true,
    },
    email: {
        type: Types.String,
        required: true,
    },
    isRemoved: {
        type: Types.Boolean,
        default: false,
        index: true,
    },
    createdAt: {
        type: Types.Date,
        default: Date.now,
    },
    updatedAt: {
        type: Types.Date,
        default: Date.now,
    },
}, {
    minimize: false,
});

// No time to find out how to map fields in Graphql, mapped it via mongoose
UsersSchema.virtual<IDocument>('photoUrl').get(function() {
    return getUserPhotoUrl(this.photo);
});

UsersSchema.pre<IDocument>('save', function() {
    this.updatedAt = new Date();
});

UsersSchema.statics.findByQuery = async function(query: IGetQuery): Promise<IFindByQueryResult<IObject>> {
    const [res] = await this
        .aggregate()
        .match({
            isRemoved: false,
        })
        .sort({
            createdAt: -1,
        })
        .facet({
            users: [
                {
                    $skip: query.skip,
                },
                {
                    $limit: query.limit,
                },
            ],
            total: [
                {
                    $count: 'total',
                },
            ],
        })
        .exec();

    return {
        users: res.users.map((user) => {
            // Same map staff
            return {
                ...user,
                id: user._id,
                photoUrl: getUserPhotoUrl(user.photo),
            };
        }),
        total: Mongo.extractAggregationResult(res, 'total', 0),
    };
};

export const Users = model<IDocument, IModel>(
    USERS_COLLECTION_NAME,
    UsersSchema,
    USERS_COLLECTION_NAME
);
