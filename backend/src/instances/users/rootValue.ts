import {
    IGetOnePath,
    IGetQuery,
    IPatchBody,
    IPostBody,
    IRemovePath,
} from './types/requests';
import {
    IGet,
    IGetOne,
    IPatch,
    IPost,
    IRemove,
} from './types/responses';

import {
    ObjectId,
} from 'mongodb';

import {
    RESPONSE_ERROR,
} from './constants';
import {
    Users,
} from './model';
import * as validators from './validators';

export default {
    users: async (query: IGetQuery): Promise<IGet> => {
        try {
            return Users.findByQuery(query);
        } catch (e) {
            return {
                error: `${e}`,
            };
        }
    },
    user: async (path: IGetOnePath): Promise<IGetOne> => {
        if (!ObjectId.isValid(path.id)) {
            return {
                error: RESPONSE_ERROR.INVALID_ID,
            };
        }

        const user = await Users.findById(path.id);

        if (!user) {
            return {
                error: RESPONSE_ERROR.NOT_FOUND,
            };
        }

        return {
            user,
        };
    },
    addUser: async (body: IPostBody): Promise<IPost> => {
        const validationError = await validators.validatePostBody(body);

        if (validationError) {
            return {
                error: validationError,
            };
        }

        const user = new Users(body);

        try {
            await user.save();

            return {
                user,
            };
        } catch (e) {
            return {
                error: `${e}`,
            };
        }
    },
    updateUser: async (body: IPatchBody): Promise<IPatch> => {
        const validationError = await validators.validatePatchBody(body);

        if (validationError) {
            return {
                error: validationError,
            };
        }

        const user = await Users.findById(body.id);

        if (!user) {
            return {
                error: RESPONSE_ERROR.NOT_FOUND,
            };
        }

        user.set(body);

        try {
            await user.save();

            return {
                user,
            };
        } catch (e) {
            return {
                error: `${e}`,
            };
        }
    },
    removeUser: async (path: IRemovePath): Promise<IRemove> => {
        if (!ObjectId.isValid(path.id)) {
            return {
                error: RESPONSE_ERROR.INVALID_ID,
            };
        }

        const user = await Users.findById(path.id);

        if (!user) {
            return {
                error: RESPONSE_ERROR.NOT_FOUND,
            };
        }

        user.set({
            isRemoved: true,
        });

        try {
            await user.save();

            return {};
        } catch (e) {
            return {
                error: `${e}`,
            };
        }
    },
};
