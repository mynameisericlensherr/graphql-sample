import {
    buildSchema,
} from 'graphql';

export default buildSchema(`
    type Query {
        users(skip: Int!, limit: Int!): GetUsersRes!
        user(id: ID!): GetUserRes!
    }
    type Mutation {
        addUser(name: String!, photo: ID!, email: String!): AddUserRes!
        updateUser(id: ID!, name: String!, photo: ID!, email: String!): UpdateUserRes!
        removeUser(id: ID!): RemoveUserRes!
    }
    type User {
        id: ID!
        name: String!
        photo: ID!
        email: String!
        
        photoUrl: String!
    }
    type GetUsersRes {
        users: [User]
        total: Int
        error: String
    }
    type GetUserRes {
        user: User
        error: String
    }
    type AddUserRes {
        user: User
        error: String
    }
    type UpdateUserRes {
        user: User
        error: String
    }
    type RemoveUserRes {
        error: String
    }
`);
