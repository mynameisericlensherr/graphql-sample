import {
    ObjectId,
} from 'mongodb';

export interface IGetQuery {
    skip: number;
    limit: number;
}

export interface IGetOnePath {
    id: ObjectId;
}

export interface IPostBody {
    name: string;
    photo: ObjectId;
    email: string;
}

export interface IPatchBody {
    id: ObjectId;

    name: string;
    photo: ObjectId;
    email: string;
}

export interface IRemovePath {
    id: ObjectId;
}
