import {
    IWithError,
} from 'instances/types';

import {
    IObject,
} from '.';

export interface IGet extends IWithError {
    users?: IObject[];
    total?: number;
}

export interface IGetOne extends IWithError {
    user?: IObject;
}

export interface IPost extends IWithError {
    user?: IObject;
}

export interface IPatch extends IWithError {
    user?: IObject;
}

export interface IRemove extends IWithError {

}
