import {
    ObjectId,
} from 'mongodb';
import {
    Document, Model,
} from 'mongoose';

import {
    IGetQuery,
} from './requests';

export interface IMongoose {
    name: string;
    photo: ObjectId;
    email: string;
}

export interface IObject extends IMongoose {
    _id: ObjectId;
    id: ObjectId;

    photoUrl: string;

    isRemoved: boolean;

    createdAt: Date;
    updatedAt: Date;
}

export interface IDocument extends IObject, Document {
    _id: ObjectId;
    id: ObjectId;
}

export interface IFindByQueryResult<IObject> {
    users: IObject[];
    total: number;
}

export interface IModel extends Model<IDocument> {
    findByQuery(query: IGetQuery): Promise<IFindByQueryResult<IObject>>;
}
