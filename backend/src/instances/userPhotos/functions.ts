import {
    IObject,
} from './types';

import {
    ObjectId,
} from 'mongodb';

import config from 'config';

import {
    USER_PHOTOS_PATH,
} from './constants';

export function getStaticName(userPhoto: IObject): string {
    return `${userPhoto._id}_${userPhoto.name}`;
}

export function getUserPhotoUrl(id: ObjectId): string {
    return `${config.BACKEND_URL}/api${USER_PHOTOS_PATH}/${id}`;
}
