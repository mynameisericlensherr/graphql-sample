import {
    RESPONSE_ERROR,
} from './constants';

export async function validatePostByUserBody(file: Express.Multer.File): Promise<RESPONSE_ERROR | null> {
    if (!/(.jpg|.jpeg|.png)$/.test(file.originalname)) {
        return RESPONSE_ERROR.INVALID_EXTENSION;
    }

    return null;
}
