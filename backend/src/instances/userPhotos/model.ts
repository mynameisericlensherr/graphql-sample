import {
    IDocument,
    IModel,
} from './types';

import {
    Response,
} from 'express';
import {
    ObjectId,
} from 'mongodb';
import {
    model,
    Schema,
} from 'mongoose';
import path from 'path';

import Files from 'tools/Files';

import {
    USER_PHOTOS_COLLECTION_NAME,
} from './constants';
import {
    getStaticName,
} from './functions';

const {
    Types,
} = Schema;

const UserPhotosSchema = new Schema<IDocument, IModel>({
    name: {
        type: Types.String,
        required: true,
    },
    size: {
        type: Types.Number,
        required: true,
    },

    createdAt: {
        type: Types.Date,
        default: Date.now,
    },
    updatedAt: {
        type: Types.Date,
        default: Date.now,
    },
}, {
    minimize: false,
});

UserPhotosSchema.pre<IDocument>('save', function() {
    this.updatedAt = new Date();
});

UserPhotosSchema.statics.existsById = async function(id: ObjectId): Promise<boolean> {
    return !!await this.exists({
        _id: id,
    });
};

UserPhotosSchema.methods.getPhotoPipe = function(this: IDocument, res: Response): void {
    Files.readFilePipe(path.join(__dirname, 'static', getStaticName(this)), res);
};

UserPhotosSchema.methods.savePhoto = async function(this: IDocument, image: Buffer): Promise<void> {
    return await Files.writeFileBuffer(path.join(__dirname, 'static', getStaticName(this)), image);
};

export const UserPhotos = model<IDocument, IModel>(
    USER_PHOTOS_COLLECTION_NAME,
    UserPhotosSchema,
    USER_PHOTOS_COLLECTION_NAME
);
