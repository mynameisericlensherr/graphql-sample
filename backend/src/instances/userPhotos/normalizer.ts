import {
    ILocals,
    IObject,
} from './types';
import {
    INorm,
} from './types/normalizer';

import {
    Request,
    Response,
} from 'express';

import {
    RESPONSE_ERROR,
} from './constants';
import {
    getUserPhotoUrl,
} from './functions';

export function normalizeUserPhoto(userPhoto: IObject): INorm {
    return {
        id: userPhoto._id,

        url: getUserPhotoUrl(userPhoto._id),
    };
}

export function normalizer(req: Request, res: Response) {
    const {
        userPhoto,
    } = res.locals as ILocals;

    if (userPhoto) {
        res.status(200);
        res.send({
            userPhoto: normalizeUserPhoto(userPhoto),
        });

        return;
    }

    res.status(500);
    res.send({
        error: RESPONSE_ERROR.NOTHING_TO_NORMALIZE,
    });
}
