import {
    NextFunction,
    Request,
    Response,
} from 'express';

import {
    RESPONSE_ERROR,
} from './constants';
import {
    UserPhotos,
} from './model';
import * as validators from './validators';

export async function getByUser(req: Request, res: Response) {
    const {
        pathId,
    } = res.locals;
    const userPhoto = await UserPhotos.findById(pathId);

    if (!userPhoto) {
        res.status(404);
        res.send({
            error: RESPONSE_ERROR.NOT_FOUND,
        });

        return;
    }

    try {
        userPhoto.getPhotoPipe(res);
    } catch (e) {
        res.status(500);
        res.send({
            error: `${e}`,
        });
    }
}

export async function postByUser(req: Request, res: Response, next: NextFunction) {
    const bodyImage = req.file;

    if (!bodyImage) {
        res.status(500);
        res.send({
            error: RESPONSE_ERROR.FILE_NOT_HANDLED,
        });

        return;
    }

    const validationError = await validators.validatePostByUserBody(bodyImage);

    if (validationError) {
        res.status(400);
        res.send({
            error: validationError,
        });

        return;
    }

    const userPhoto = new UserPhotos({
        name: bodyImage.originalname,
        size: bodyImage.size,
    });

    try {
        await userPhoto.save();
        await userPhoto.savePhoto(bodyImage.buffer);

        res.locals = {
            ...res.locals,
            userPhoto,
        };
        next();
    } catch (e) {
        res.status(500);
        res.send({
            error: `${e}`,
        });
    }
}
