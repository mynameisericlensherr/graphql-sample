import {
    Router,
} from 'express';
import multer from 'multer';

import Express from 'tools/Express';

import {
    MAX_FILE_SIZE,
} from './constants';
import * as mw from './middlewares';
import {
    normalizer,
} from './normalizer';

const userPhotosRouter = Router();

const postByUserMulter = multer({
    limits: {
        fields: 1,
        fileSize: MAX_FILE_SIZE,
        files: 1,
    },
}).single('photo');

userPhotosRouter.get(
    '/:id',
    Express.checkPathIdMid,
    mw.getByUser
);

userPhotosRouter.post(
    '/',
    postByUserMulter,
    mw.postByUser,
    normalizer
);

export default userPhotosRouter;
