import {
    Response,
} from 'express';
import {
    ObjectId,
} from 'mongodb';
import {
    Document,
    Model,
} from 'mongoose';

export interface IMongoose {
    name: string;
    size: number;
}

export interface IObject extends IMongoose {
    _id: Document['_id'];

    createdAt: Date;
    updatedAt: Date;
}

export interface IDocument extends Document, IObject {
    _id: Document['_id'];

    getPhotoPipe(res: Response): void;
    savePhoto(photo: Buffer): Promise<void>;
}

export interface IModel extends Model<IDocument> {
    existsById(id: ObjectId): Promise<boolean>;
}

export interface ILocals {
    userPhoto?: IObject;
}
