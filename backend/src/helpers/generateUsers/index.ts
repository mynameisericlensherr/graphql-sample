import {
    IMongoose as IUserPhotoMongoose,
} from 'instances/userPhotos/types';
import {
    IMongoose as IUserMongoose,
} from 'instances/users/types';

import config from 'config';

import Files from 'tools/Files';
import Mongo from 'tools/Mongo';

import {
    UserPhotos,
} from 'instances/userPhotos/model';
import {
    Users,
} from 'instances/users/model';

import {
    getStaticName,
} from '../../instances/userPhotos/functions';
import {
    faker,
} from '@faker-js/faker';

export default async function() {
    await Mongo.connect(config.DB_URL);

    if (await UserPhotos.countDocuments()) {
        return;
    }

    const photoName = 'shrek.png';
    const userPhotoData: IUserPhotoMongoose = {
        name: photoName,
        size: 1,
    };
    const userPhoto = new UserPhotos(userPhotoData);

    await userPhoto.save();

    const photoBuffer = await Files.readFileBuffer(Files.joinPaths(__dirname, photoName));

    await Files.writeFileBuffer(Files.joinPaths(__dirname, '..', '..', 'instances', 'userPhotos', 'static', getStaticName(userPhoto)), photoBuffer);

    for (let i = 0; i < 100; ++i) {
        const userData: IUserMongoose = {
            name: faker.name.firstName(),
            photo: userPhoto._id,
            email: faker.internet.email(),
        };
        const user = new Users(userData);

        await user.save();
    }
}
