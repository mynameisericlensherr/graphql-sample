require('module-alias/register');

import app from 'app';

import config from 'config';

import Express from 'tools/Express';
import Files from 'tools/Files';
import Mongo from 'tools/Mongo';

import generateUsers from 'helpers/generateUsers';

(async function() {
    await Mongo.connect(config.DB_URL);
    await Files.mkdir(Files.joinPaths(__dirname, 'instances', 'userPhotos', 'static'));
    await generateUsers();

    const {
        port,
    } = Express.parseUrl(config.BACKEND_URL);

    app.listen(port, () => {
        console.log(`Express server started on port: ${port}`);
    });
})();
