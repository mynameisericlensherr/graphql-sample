import {
    Response,
} from 'express';
import fs from 'fs';
import nodePath from 'path';

class Files {
    public static readFile(file: string): Promise<string> {
        return new Promise<string>((resolve, reject) => {
            fs.readFile(file, 'utf-8', (error, data) => {
                error ? reject(error) : resolve(data);
            });
        });
    }

    public static readFileBuffer(file: string): Promise<Buffer> {
        return new Promise<Buffer>((resolve, reject) => {
            fs.readFile(file, (error, data) => {
                error ? reject(error) : resolve(data);
            });
        });
    }

    public static readFilePipe(file: string, res: Response): void {
        fs.createReadStream(file).pipe(res);
    }

    public static writeFile(file: string, data: string): Promise<void> {
        return new Promise<void>((resolve, reject) => {
            fs.writeFile(file, data, 'utf-8', (error) => {
                error ? reject(error) : resolve();
            });
        });
    }

    public static writeFileBuffer(file: string, data: Buffer): Promise<void> {
        return new Promise<void>((resolve, reject) => {
            fs.writeFile(file, data, (error) => {
                error ? reject(error) : resolve();
            });
        });
    }

    public static removeFile(file: string): Promise<void> {
        return new Promise<void>((resolve, reject) => {
            fs.unlink(file, (error) => {
                error ? reject(error) : resolve();
            });
        });
    }

    public static exists(path: string): Promise<boolean> {
        return new Promise<boolean>((resolve) => {
            fs.exists(path, (isExist) => {
                resolve(isExist);
            });
        });
    }

    public static async mkdir(path: string): Promise<void> {
        if (await Files.exists(path)) {
            return;
        }

        return new Promise<void>((resolve, reject) => {
            fs.mkdir(path, (error) => {
                if (error) {
                    reject(error);
                } else {
                    resolve();
                }
            });
        });
    }

    public static ls(path: string): Promise<string[]> {
        return new Promise<string[]>((resolve, reject) => {
            fs.readdir(path, (error, paths) => {
                if (error) {
                    reject(error);

                    return;
                }

                resolve(paths.map((dir) => Files.joinPaths(path, dir)));
            });
        });
    }

    public static joinPaths(...paths: string[]): string {
        return nodePath.join(...paths);
    }

    public static getFileName(path: string): string {
        return nodePath.basename(path);
    }
}

export default Files;
