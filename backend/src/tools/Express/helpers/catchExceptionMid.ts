import {
    NextFunction,
    Request,
    Response,
} from 'express';

export default function(exception: Error, req: Request, res: Response, next: NextFunction): void {
    const error = {
        error: {
            name: exception.name,
            message: exception.message,
        },
    };

    res.status(500);
    res.send(error);
    console.log(exception);
    next();
}
