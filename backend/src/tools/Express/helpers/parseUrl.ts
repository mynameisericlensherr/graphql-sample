import {
    URL,
} from 'url';

export default function(url: string): URL {
    return new URL(url);
}
