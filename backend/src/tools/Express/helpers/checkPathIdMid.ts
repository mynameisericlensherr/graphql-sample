import {
    NextFunction,
    Request,
    Response,
} from 'express';
import {
    ObjectId,
} from 'mongodb';

export default function(req: Request, res: Response, next: NextFunction): void {
    const {
        id,
    } = req.params;

    if (!ObjectId.isValid(id)) {
        res.status(400);
        res.send({
            error: 'INVALID PATH ID',
        });

        return;
    }

    res.locals = {
        ...res.locals,
        pathId: new ObjectId(id),
    };
    next();
}
