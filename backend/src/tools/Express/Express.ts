import {
    catchExceptionMid,
    checkPathIdMid,
    parseUrl,
} from './helpers';

class Express {
    public static checkPathIdMid = checkPathIdMid;
    public static catchExceptionMid = catchExceptionMid;
    public static parseUrl = parseUrl;
}

export default Express;
