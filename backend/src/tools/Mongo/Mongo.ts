import {
    Db,
} from 'mongodb';
import {
    ConnectOptions,
} from 'mongoose';

import {
    connect,
    extractAggregationResult,
} from './helpers';

class Mongo {
    public static db: Db | null;
    public static extractAggregationResult = extractAggregationResult;

    public static connect = async (url: string, options?: ConnectOptions): Promise<Db> => {
        Mongo.db = await connect(url, options);

        return Mongo.db;
    };
}

export default Mongo;
