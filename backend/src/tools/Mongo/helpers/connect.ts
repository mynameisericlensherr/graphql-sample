import {
    Db,
} from 'mongodb';
import mongoose, {
    ConnectOptions,
} from 'mongoose';

async function connectMongoose(url: string, options?: ConnectOptions): Promise<Db> {
    const mongooseConnection = await mongoose.connect(url, options);

    return mongooseConnection.connection.db as Db;
}

export default async function connect(url: string, options?: ConnectOptions): Promise<Db> {
    console.log('Mongoose connection with retry');

    try {
        const connection = await connectMongoose(url, options);

        console.log('Mongoose is here');

        return connection;
    } catch (error) {
        console.log(url);
        console.log(error);
        console.log('Mongoose connection unsuccessful, retry after 5 seconds.');

        return await new Promise<Db>((resolve) => {
            setTimeout(async () => {
                const connection = await connect(url, options);

                resolve(connection);
            }, 5000);
        });
    }
}
