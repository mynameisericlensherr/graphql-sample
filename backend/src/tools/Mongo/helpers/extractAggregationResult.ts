export default function<IValue>(data: any[], name: string, defaultValue: IValue): IValue {
    let item = data[name];

    item = item && item[0];

    return item && item[name] ? item[name] : defaultValue;
}
