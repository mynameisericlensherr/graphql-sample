import dotEnv from 'dotenv';
import path from 'path';

export enum CONFIG_MODE {
    DEV = 'DEV',
    PROD = 'PROD',
}

const mode = CONFIG_MODE.PROD;

dotEnv.config({
    path: path.join(__dirname, 'env', `${mode}.env`),
});

export default {
    MODE: mode,
    DB_URL: String(process.env.DB_URL),
    BACKEND_URL: String(process.env.BACKEND_URL),
};
