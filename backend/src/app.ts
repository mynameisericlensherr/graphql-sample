import bodyParser from 'body-parser';
import cookieParser from 'cookie-parser';
import cors from 'cors';
import express from 'express';
import logger from 'morgan';

import config from 'config';

import Express from 'tools/Express';

const app = express();

if (config.MODE === 'DEV') {
    app.use(logger('dev'));
}

app.use(cors());
app.use(
    bodyParser.json({
        limit: '1024mb',
        type: ['application/json', 'text/plain'],
    })
);
app.use(
    bodyParser.urlencoded({
        limit: '1024mb',
        extended: true,
    })
);
app.use(cookieParser());
app.use(express.json());
app.use(express.urlencoded({
    extended: false,
}));

import router from './router';

app.use('/api', router);
app.use(express.static('./public'));

app.get('*', function(req, res) {
    res.sendFile('./public/index.html', {
        root: '.',
    });
});

app.use(Express.catchExceptionMid);

export default app;
